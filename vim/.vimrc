

" ##########################
"  GENRAL SETTINGS
" ##########################

" Set the <Leader> key to comma instead of the default backslash
let mapleader = ','

" Use latest Vim options and not pure vi.
set nocompatible

" Location of VIM swap files
set directory=$HOME/temp.tmp/vim-swapfiles//




" ##########################
"  EDITING SETTINGS
" ##########################

" Set backspace to work as in other editors. Delete more simbols than inserted.
set backspace=indent,eol,start

" Navigation in insert mode
imap <C-h>  <Left>
imap <C-l>  <Right>
imap <C-j>  <Down>
imap <C-k>  <Up>



" ##########################
"  VIM WINDOWS
" ##########################

" Ensure that horizontal split wil always be below
set splitbelow
" Ensure that vertical split will alwasy be on the right
set splitright

nmap <C-J> <C-W><C-J>
nmap <C-H> <C-W><C-H>
nmap <C-K> <C-W><C-K>
nmap <C-L> <C-W><C-L>




"--------------COPY/PASTE system register -------------------"
nmap <Leader>cc     "+yy
nmap <Leader>cp     "+p



" Settings for gvim - font and space between lines
set guifont=Monospace\ Regular\ 18
set linespace=5


" Plugin Manager 
call plug#begin('~/.vim/plugged')

Plug 'NLKNguyen/papercolor-theme'

" Add plugins to &runtimepath
call plug#end()

" VIM support for 256 colors. 
" The TERM shell variable must show that the terminal supports 256 colors.
" a) screen-256colors; b) xterm-256color
set t_Co=256
colorscheme PaperColor
